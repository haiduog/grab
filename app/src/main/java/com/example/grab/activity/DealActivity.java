package com.example.grab.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.grab.R;

public class DealActivity extends AppCompatActivity implements View.OnClickListener {
    Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deal);
        intent = new Intent(DealActivity.this, DealDetail.class);
        findViewById(R.id.deal_20).setOnClickListener(this);
        findViewById(R.id.deal_30).setOnClickListener(this);
        findViewById(R.id.deal_50).setOnClickListener(this);
        findViewById(R.id.tv_header).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.deal_20:
                intent.putExtra("DEAL", "20.000");
                break;
            case R.id.deal_30:
                intent.putExtra("DEAL", "30.000");
                break;
            case R.id.deal_50:
                intent.putExtra("DEAL", "50.000");
                break;
        }
        startActivity(intent);
    }
}