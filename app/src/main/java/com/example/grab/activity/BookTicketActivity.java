package com.example.grab.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.grab.Adapter.ViTriAdapter;
import com.example.grab.R;

import java.util.ArrayList;

public class BookTicketActivity extends AppCompatActivity {
    Button button;
    ArrayList<Integer> numbers;
    ViTriAdapter adapter;
    RecyclerView recyclerView,recyclerView2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_ticket);
        recyclerView = findViewById(R.id.rcl_view);
        recyclerView2 = findViewById(R.id.rcl_view2);
        numbers = new ArrayList<>();
        for(int i = 1;i<13;i++){
            numbers.add(i);
        }
        adapter = new ViTriAdapter(numbers, this);
        recyclerView.setAdapter(adapter);
        recyclerView2.setAdapter(adapter);
        button = findViewById(R.id.btn);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(BookTicketActivity.this, SuccessActivity.class));
            }
        });
        findViewById(R.id.tv_header).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }
}