package com.example.grab.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.grab.R;

public class DealDetail extends AppCompatActivity {
    TextView title, describe;
    ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deal_detail);
        title = findViewById(R.id.title);
        describe = findViewById(R.id.descibe);
        imageView = findViewById(R.id.image);
        String deal = getIntent().getStringExtra("DEAL");
        title.setText(String.format(getString(R.string.title_detail), deal));
        describe.setText(String.format(getString(R.string.describe_detail), deal));
        findViewById(R.id.tv_header).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }
}