package com.example.grab.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.View;

import com.example.grab.R;
import com.example.grab.fragment.AccountFragment;
import com.example.grab.fragment.FavoriteFragment;
import com.example.grab.fragment.HomeFragment;
import com.example.grab.fragment.LoginFragment;
import com.example.grab.fragment.RegisterFragment;
import com.example.grab.fragment.TicketFragment;
import com.google.android.material.tabs.TabLayout;

public class MainActivity extends AppCompatActivity {

    public interface OnRefe{
        void onShow();
    }
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private int[] tabIcons = {
            R.drawable.ic_home,
            R.drawable.ic_ticket,
            R.drawable.ic_unheart,
            R.drawable.account,
    };
    private int[] tabIconsSelected = {
            R.drawable.home,
            R.drawable.ticket,
            R.drawable.ic_heart,
            R.drawable.account2
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tabLayout = findViewById(R.id.tab_home);
        viewPager = findViewById(R.id.vp_home);
        tabLayout.setTabMode(TabLayout.MODE_FIXED);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        addControl();
        setupTabIcons();
    }
    private void addControl() {
        FragmentManager manager = getSupportFragmentManager();
        PagerAdapter adapter = new PagerAdapter(manager);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setTabsFromPagerAdapter(adapter);//deprecated
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager));
        viewPager.setOffscreenPageLimit(4);
    }
    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(getResources().getDrawable(R.drawable.selector_home));
        tabLayout.getTabAt(1).setIcon(getResources().getDrawable(R.drawable.selector_ticket));
        tabLayout.getTabAt(2).setIcon(getResources().getDrawable(R.drawable.selector_fav));
        tabLayout.getTabAt(3).setIcon(getResources().getDrawable(R.drawable.selector_account));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition()==0){
                    HomeFragment.llTK.setVisibility(View.GONE);
                    HomeFragment.llDefault.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                if (tab.getPosition()==0){
                    HomeFragment.llTK.setVisibility(View.GONE);
                    HomeFragment.llDefault.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    public class PagerAdapter extends FragmentStatePagerAdapter {

        PagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }
        @Override
        public Fragment getItem(int position) {
            Fragment frag=null;
            switch (position){
                case 0:
                    frag = new HomeFragment();
                    break;
                case 1:
                    frag = new TicketFragment();
                    break;
                case 2:
                    frag = new FavoriteFragment();
                    break;
                case 3:
                    frag = new AccountFragment();
                    break;
            }
            return frag;
        }

        @Override
        public int getCount() {
            return 4;
        }
        @Override
        public CharSequence getPageTitle(int position) {
            String title = "";

            return title;
        }
    }
}