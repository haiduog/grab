package com.example.grab.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.grab.R;

import java.util.ArrayList;
import java.util.List;

public class DistrictAdapter extends RecyclerView.Adapter<DistrictAdapter.DistrictViewHolder> {
    private List<Integer> districts;

    public DistrictAdapter() {
        this.districts = new ArrayList<>();
        districts.add(R.drawable.an1);
        districts.add(R.drawable.a2);
        districts.add(R.drawable.a3);
        districts.add(R.drawable.a1);
    }

    @NonNull
    @Override
    public DistrictViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return new DistrictViewHolder((LayoutInflater.from(parent.getContext())).inflate(R.layout.item_image, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull DistrictViewHolder holder, final int position) {
        holder.imageView.setImageResource(districts.get(position));
    }

    @Override
    public int getItemCount() {
        return districts.size();
    }


    public class DistrictViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        public DistrictViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.image);
        }
    }
}
