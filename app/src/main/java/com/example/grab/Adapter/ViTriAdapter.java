package com.example.grab.Adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.example.grab.R;

import java.util.ArrayList;
import java.util.Random;
import java.util.zip.Inflater;

public class ViTriAdapter extends RecyclerView.Adapter<ViTriAdapter.Holder> {
    ArrayList<Integer> numbers;
    Context context;

    public ViTriAdapter(ArrayList<Integer> numbers, Context context) {
        this.numbers = numbers;
        this.context = context;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return new Holder(LayoutInflater.from(context).inflate(R.layout.item_vi_tri, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        holder.bin(position);
    }

    @Override
    public int getItemCount() {
        return numbers.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView textView;
        RelativeLayout relativeLayout;

        public Holder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.vi_tri);
            relativeLayout = itemView.findViewById(R.id.parent);
        }

        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        public void bin(int position) {
            textView.setText(numbers.get(position) + "");
            Random random = new Random();
            Drawable layout = null;
            int num = random.nextInt(3);
            switch (num) {
                case 0:
                    layout = context.getDrawable(R.drawable.item);
                    break;
                case 1:
                    layout = context.getDrawable(R.drawable.item2);
                    break;
                case 2:
                    layout = context.getDrawable(R.drawable.item3);
                    break;
            }

            relativeLayout.setBackground(layout);

        }
    }
}
