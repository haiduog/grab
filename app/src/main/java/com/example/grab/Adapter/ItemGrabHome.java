package com.example.grab.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.grab.R;
import com.example.grab.activity.DetailActivity;

import java.util.ArrayList;
import java.util.List;

public class ItemGrabHome extends RecyclerView.Adapter<ItemGrabHome.ViewHolder> {
    List<String> list = new ArrayList<>();
    private int type;
    private Activity mContext;

    public ItemGrabHome(int type, Activity context) {
        list.add("Bắc Ninh");
        list.add("Nghệ An");
        list.add("Quảng Ninh");
        list.add("Yên Bái");
        list.add("Thái Bình");
        list.add("Ninh Bình");
        this.type = type;
        mContext = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (type == 0)
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_oto_home, parent, false));
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_oto_home_2, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (type == 1){
            holder.name.setText(list.get(position));
            Display display = mContext.getWindowManager().getDefaultDisplay();
            int width = ((display.getWidth()*33)/100);
            ConstraintLayout.LayoutParams parms = new ConstraintLayout.LayoutParams(width, ViewGroup.LayoutParams.MATCH_PARENT);
            holder.iv.setLayoutParams(parms);
        }

        if (type == 0){
            holder.name.setText("Hà Nội-" + list.get(position));
            Display display = mContext.getWindowManager().getDefaultDisplay();
            int width = ((display.getWidth()*30)/100);
            LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(width, ViewGroup.LayoutParams.MATCH_PARENT);
            holder.iv.setLayoutParams(parms);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mContext.startActivity(new Intent(mContext, DetailActivity.class));
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        ImageView iv;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.tv_name);
            iv = itemView.findViewById(R.id.item_img);
        }
    }
}
