package com.example.grab.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.grab.DialogConfirm;
import com.example.grab.R;
import com.example.grab.activity.DetailActivity;

public class GrabAdapter extends RecyclerView.Adapter<GrabAdapter.ViewHolder> {
    private int type;
    private int like;
    private boolean isShowCancel;
    private Context mContext;

    public GrabAdapter(int type, int like, Context context) {
        this.type = type;
        this.like = like;
        mContext = context;
    }
    public GrabAdapter(int type, int like, Context context, boolean isShowCancel) {
        this.type = type;
        this.like = like;
        this.isShowCancel = isShowCancel;
        mContext = context;
    }

    public interface IOnItemClick{
         void onItemClick(int position);
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_oto,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        if (type == 0)
            holder.imgHeader.setImageResource(R.drawable.oto2);
        else holder.imgHeader.setImageResource(R.drawable.oto);
        if (like == 0)
            holder.imgHeart.setImageResource(R.drawable.unheart);
        else {
            holder.imgHeart.setVisibility(View.VISIBLE);
            holder.imgDelete.setVisibility(View.GONE);
            holder.imgHeart.setImageResource(R.drawable.heart);
        }
        if(isShowCancel){
            holder.btnCancel.setVisibility(View.VISIBLE);
        }else{
            holder.btnCancel.setVisibility(View.GONE);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mContext.startActivity(new Intent(mContext, DetailActivity.class));
            }
        });
        holder.btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogConfirm dialogConfirm = new DialogConfirm(mContext, true);
                        dialogConfirm.setCanceledOnTouchOutside(false);
                        dialogConfirm.show();

            }
        });
        holder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogConfirm dialogConfirm = new DialogConfirm(mContext, false);
                dialogConfirm.setCanceledOnTouchOutside(false);
                dialogConfirm.show();

            }
        });
    }

    @Override
    public int getItemCount() {
        return 10;
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgHeart,imgHeader, imgDelete;
        private Button btnCancel;
        ConstraintLayout parent;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imgDelete = itemView.findViewById(R.id.img_delete);
            imgHeader = itemView.findViewById(R.id.item_img);
            imgHeart = itemView.findViewById(R.id.img_heart);
            btnCancel = itemView.findViewById(R.id.btn_cancel);
            parent = itemView.findViewById(R.id.item);
        }
    }
}
