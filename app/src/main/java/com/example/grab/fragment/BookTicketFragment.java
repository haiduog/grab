package com.example.grab.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.grab.Adapter.GrabAdapter;
import com.example.grab.R;


public class BookTicketFragment extends Fragment {

    private RecyclerView rvBookTicket;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_book_ticket, container, false);
        rvBookTicket = view.findViewById(R.id.rvBookTicket);
        rvBookTicket.setAdapter(new GrabAdapter(1,0,getContext(), true));
        return view;
    }
}