package com.example.grab.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.grab.R;
import com.example.grab.activity.LoginActivity;
import com.example.grab.activity.MainActivity;


public class RegisterFragment extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_register, container, false);
        view.findViewById(R.id.btn_regist).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog("Bạn đã đăng ký \n thành công!");
            }
        });
        return view;

    }
    private void showDialog(String text){
        final Dialog dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.dialog_default);
        dialog.setCancelable(false);
        TextView textView = dialog.findViewById(R.id.tv_content);
        textView.setText(text);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.findViewById(R.id.btn_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
                LoginActivity.viewPager.setCurrentItem(1);
            }
        });
        dialog.show();
    }
}