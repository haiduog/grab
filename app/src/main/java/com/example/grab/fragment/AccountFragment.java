package com.example.grab.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.grab.R;
import com.example.grab.activity.AccountInfomation;
import com.example.grab.activity.ChangePasswordActivity;
import com.example.grab.activity.DealActivity;
import com.example.grab.activity.LoginActivity;
import com.example.grab.activity.NewsActivity;
import com.example.grab.activity.RuleActivity;


public class AccountFragment extends Fragment implements View.OnClickListener {


    LinearLayout llLogin, llNotLogin, llLoginControll, llLoginNotControll;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_account, container, false);

        view.findViewById(R.id.thay_doi_thong_tin).setOnClickListener(this);
        view.findViewById(R.id.doi_mat_khau).setOnClickListener(this);
        view.findViewById(R.id.uu_dai).setOnClickListener(this);
        view.findViewById(R.id.tin_xe_khach).setOnClickListener(this);
        view.findViewById(R.id.dang_xuat).setOnClickListener(this);
        view.findViewById(R.id.chinh_sach).setOnClickListener(this);
        llLogin = view.findViewById(R.id.ll_login);
        llLoginControll = view.findViewById(R.id.ll_login_controll);
        llNotLogin = view.findViewById(R.id.ll_not_login);
        llLoginNotControll = view.findViewById(R.id.ll_not_login_controll);
        llNotLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), LoginActivity.class));
            }
        });
        return view;

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.thay_doi_thong_tin:
                startActivity(new Intent(getActivity(), AccountInfomation.class));
                return;
            case R.id.doi_mat_khau:
                startActivity(new Intent(getActivity(), ChangePasswordActivity.class));
                return;
            case R.id.uu_dai:
                startActivity(new Intent(getActivity(), DealActivity.class));
                return;
            case R.id.tin_xe_khach:
               // startActivity(new Intent(getActivity(), NewsActivity.class));
                return;
            case R.id.dang_xuat:
                showDialog();
                return;
            case R.id.chinh_sach:
                startActivity(new Intent(getActivity(), RuleActivity.class));
        }
    }
    private void showDialog(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Bạn có chắc chắn muốn đăng xuất không?");
        builder.setPositiveButton("Huỷ", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.setNegativeButton("Đồng ý".toUpperCase(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dangXuat();
            }
        });
        AlertDialog dialog =  builder.create();
        dialog.show();

    }
    void dangXuat() {
        llNotLogin.setVisibility(View.VISIBLE);
        llLoginNotControll.setVisibility(View.VISIBLE);
        llLogin.setVisibility(View.GONE);
        llLoginControll.setVisibility(View.GONE);
    }
}