package com.example.grab.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.grab.Adapter.GrabAdapter;
import com.example.grab.activity.DetailActivity;
import com.example.grab.R;


public class FavoriteFragment extends Fragment implements GrabAdapter.IOnItemClick {

    private RecyclerView rvFav;
    private GrabAdapter adapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.fragment_favorite, container, false);
            rvFav = view.findViewById(R.id.rvFav);
            adapter = new GrabAdapter(1,1, getContext(), false);
            rvFav.setAdapter(adapter);
            return view;
    }

    @Override
    public void onItemClick(int position) {
        startActivity(new Intent(getContext(), DetailActivity.class));
    }
}