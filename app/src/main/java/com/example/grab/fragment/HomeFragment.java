package com.example.grab.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.grab.Adapter.DistrictAdapter;
import com.example.grab.Adapter.ItemGrabHome;
import com.example.grab.R;
import com.example.grab.activity.ListGrabActivity;


public class HomeFragment extends Fragment implements View.OnClickListener {


    private ViewPager2 viewPager;
    private RecyclerView rvFav,rvPopu,rvDaily;
    private TextView textView;
    public static LinearLayout llDefault,llTK;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        final Handler handler = new Handler();
        rvFav = view.findViewById(R.id.rv_fav_home);
        rvPopu = view.findViewById(R.id.rv_popu_home);
        rvDaily = view.findViewById(R.id.rv_daily);
        llDefault = view.findViewById(R.id.ll_default);
        llTK = view.findViewById(R.id.ll_tk);
        viewPager = view.findViewById(R.id.vp_image);
        textView = view.findViewById(R.id.tv_tk);
        viewPager.setAdapter(new DistrictAdapter());
        viewPager.setClipChildren(false);
        viewPager.setClipToPadding(false);
        viewPager.getChildAt(0).setOverScrollMode(RecyclerView.OVER_SCROLL_NEVER);
        viewPager.setCurrentItem(viewPager.getCurrentItem(), true);
        viewPager.setOnClickListener(this);
        viewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {

                handler.removeCallbacks(runnable);
                handler.postDelayed(runnable, 3000);
            }
        });
        //setup recycle view
        rvFav.setAdapter(new ItemGrabHome(0,getActivity()));
        rvPopu.setAdapter(new ItemGrabHome(0,getActivity()));
        rvDaily.setAdapter(new ItemGrabHome(1,getActivity()));
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                llDefault.setVisibility(View.GONE);
                llTK.setVisibility(View.VISIBLE);
            }
        });
        view.findViewById(R.id.btn_tk).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), ListGrabActivity.class));
            }
        });
        return view;
    }
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            viewPager.setCurrentItem(viewPager.getCurrentItem() == 3 ? 0 : viewPager.getCurrentItem() + 1);
        }
    };
    @Override
    public void onClick(View view) {

    }
}