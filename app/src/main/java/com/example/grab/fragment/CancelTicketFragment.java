package com.example.grab.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.grab.Adapter.GrabAdapter;
import com.example.grab.R;


public class CancelTicketFragment extends Fragment {

    private RecyclerView rvCancelTicket;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_cancel_ticket, container, false);
        rvCancelTicket = view.findViewById(R.id.rvCancelTicket);
        rvCancelTicket.setAdapter(new GrabAdapter(0,0, getContext(), false));
        return view;
    }
}