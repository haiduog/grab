package com.example.grab;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.solver.GoalRow;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.grab.activity.LoginActivity;

public class DialogConfirm extends Dialog {
    TextView btnAgree, btnCancel, message, textView;
    CheckBox checkBox;
    ImageView imgCancel;
    boolean isCancel;
    public DialogConfirm(@NonNull Context context, boolean isCancel) {
        super(context);
        this.isCancel = isCancel;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog_confirm);
        btnAgree = findViewById(R.id.btn_agree);
        btnCancel = findViewById(R.id.btn_cancel);
        checkBox = findViewById(R.id.checkbox);
        message = findViewById(R.id.tv_content);
        imgCancel = findViewById(R.id.btn_close);
        if(!isCancel){
            checkBox.setVisibility(View.GONE);
            message.setText("Bạn có chắc chắn muốn xóa");
        }
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                btnAgree.setClickable(isChecked);
            }
        });
        btnAgree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkBox.setVisibility(View.GONE);
                btnAgree.setVisibility(View.GONE);
                btnCancel.setVisibility(View.GONE);
                String text;
                if(!isCancel){
                    text = "Bạn đã xóa vé thành công!";
                }else{
                    text = "Bạn đã hủy vé thành công!";
                }
                message.setText(text);
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        imgCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    public void hideCheckBox() {
        checkBox.setVisibility(View.GONE);
        message.setText("Bạn có chắc chắn muốn xóa");
        btnAgree.setClickable(true);
    }


}